Liste des fonctions :
exploration : sert à faire marcher le système sur la carte dans le jeu
combat : sert à faire marcher le système de combat dans le jeu
capture : sert à faire marcher le système de capture de pokemon dans le jeu
draw1 : dessine le mode exploration
draw2 : dessine le mode combat
draw3 : dessine le mode capture 
update : actualise les variables 30 fois par seconde
draw : affiche ce qu'il faut 30 fois par seconde